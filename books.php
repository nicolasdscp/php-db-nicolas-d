<?php
include 'controllers/books.php';

session_start();

// Unlogger user prevent
if (!$_SESSION['logged']) header('Location: /login.php');

$params = array();
$method = isset($_GET['method']) ? $_GET['method'] : 'GET';

if ($method == 'GET') {
  if (isset($_GET['title']) && !empty($_GET['title'])) $params['title'] = $_GET['title'];
  if (isset($_GET['author']) && !empty($_GET['author'])) $params['author'] = $_GET['author'];
  if (isset($_GET['publisher']) && !empty($_GET['publisher'])) $params['publisher'] = $_GET['publisher'];
  if (isset($_GET['year']) && !empty($_GET['year']) && is_numeric($_GET['year'])) $params['year'] = intval($_GET['year']);

  GetBooks($params);
} 
// Admin only can manage books
elseif ($_SESSION['privilege'] == '1') {
  if ($method == 'DELETE') {
    if (isset($_GET['id']) && !empty($_GET['id']) && is_numeric($_GET['id'])) DeleteBook($_GET['id']);
  } elseif ($method == 'POST') {
    $params['title'] = empty($_GET['title']) ? '' : $_GET['title'];
    $params['author'] = empty($_GET['author']) ? '' : $_GET['author'];
    $params['publisher'] = empty($_GET['publisher']) ? '' : $_GET['publisher'];
    $params['year'] = empty($_GET['year']) ? 0 : $_GET['year'];
    CreateBook($params);
  }
}