<?php
// Prevent direct access
if (explode('?', $_SERVER['REQUEST_URI'])[0] == '/config.php') header('Location: /');

// Connection informations
const DB_HOST = 'localhost';
const DB_NAME = 'tplamp';

// Specific created user with CRUD privileges
const DB_USR = 'tplampusr';
const DB_PASS = 'tplampusr';

$GLOBALS['CON_STR'] = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
