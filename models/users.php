<?php

// Prevent direct access
if (explode('?', $_SERVER['REQUEST_URI'])[0] == 'models/books.php') header('Location: /');

include 'db.php';

// Check if a user with given login exist in DB
function DBCheckUser($login)
{
  $pdo = InitDB();
  $stms = $pdo->prepare("SELECT login, privilege FROM users WHERE login='${login}'");
  $stms->execute();
  return $stms->fetchAll(PDO::FETCH_ASSOC);
}
