<?php

// Prevent direct access
if (explode('?', $_SERVER['REQUEST_URI'])[0] == 'models/books.php') header('Location: /');

include 'db.php';

// Return all bokks in DB
function DBGetAllBooks()
{
  $pdo = InitDB();
  $stms = $pdo->prepare('SELECT * FROM books');
  $stms->execute();
  return $stms->fetchAll(PDO::FETCH_ASSOC);
}

// Search book with all parameters
function DBSearchBooks($params)
{
  // Request building
  $getBooks = 'SELECT * FROM books';
  if (count($params) >= 1) $getBooks .= ' WHERE ';
  $cpt = 0;
  foreach ($params as $key => $param) {
    if ($cpt > 0) $getBooks .= ' AND ';

    if (gettype($param) == "string") {
      $getBooks .= $key . ' LIKE "%' . $param . '%"';
    } else {
      $getBooks .= $key . "=${param}";
    }
    $cpt++;
  }
  $getBooks .= ' ORDER BY title, author DESC';
  $pdo = InitDB();
  $stms = $pdo->prepare($getBooks);
  $stms->execute($params);
  return $stms->fetchAll(PDO::FETCH_ASSOC);
}

// Delete book with given ID
function DBDeleteBook($bookID)
{
  $pdo = InitDB();
  $stms = $pdo->prepare('DELETE FROM books WHERE id=' . strval($bookID));
  $stms->execute();
}

// Create books with given paramaters
function DBCreateBook($params)
{
  $pdo = InitDB();
  $stms = $pdo->prepare("INSERT INTO books (title, author, publisher, year) VALUES (:title, :author, :publisher, :year)");
  $stms->execute($params);
}
