<?php

// Prevent direct access
if (explode('?', $_SERVER['REQUEST_URI'])[0] == 'models/publisher.php') header('Location: /');

include 'db.php';

// Return all publishers
function DBGetAllPublisher()
{
  $pdo = InitDB();
  $stms = $pdo->prepare('SELECT * FROM publisher');
  $stms->execute();
  return $stms->fetchAll(PDO::FETCH_ASSOC);
}
