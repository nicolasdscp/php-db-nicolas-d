<?php

include 'controllers/users.php';

session_start();

// Check user to validate session
if (isset($_GET['login']) && !empty($_GET['login'])) {
  CheckUser($_GET['login']);
}

// destroy session on /login.php?logout=
if (isset($_GET['logout'])) session_destroy();
// else redirect to home page
else if (isset($_SESSION['logged']) && $_SESSION['logged']) header('Location: /');

?>
<!doctype html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Login</title>
  <link rel="stylesheet" href="css/bootstrap.css">
</head>

<body>
  <div style="width: 300px; margin-left: 20px; margin-bottom: 50px; display: inline-block;">
    User simple: dev <br>
    Admin: admin <br>
    <form action="/login.php">
      <label for="login">Login</label>
      <input type="text" class="form-control" id="login" name="login">
      <button id="connect" type="submit" class="btn btn-primary">Connexion</button>
    </form>
  </div>
</body>