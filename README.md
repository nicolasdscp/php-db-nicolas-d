# Compte rendu TP PHP SQL

* Nicolas Descamps 
* AP3
* 2020/02/21

# Exercise 0
No questions

# Exercise 1.1

### 5. Which SQL request can allow us to display title and author for each book?

> The following request `'SELECT title, author FROM books'`

# Exercise 1.3

### 1.  Which would be the requests to insert and delete a book in the base ?
> INSERT: `'INSERT INTO books (title, author, publisher, year) VALUES ('val1', 'val2', 'val3', 'val4')'`

> DELETE: `'DELETE FROM books WHERE ...'`

