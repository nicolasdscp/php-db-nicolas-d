function getBooks(params = "") {
  $('#bookList > tbody').empty()
  axios.get('/books.php?method=GET&' + params)
  .then(res => {
    res.data.forEach(book => {
      $('#bookList > tbody').append(`
        <tr id="book_${book.id}">
          <td>${book.id}</td>
          <td>${book.title}</td>
          <td>${book.author}</td>
          <td>${book.publisher}</td>
          <td>${book.year}</td>
          <td><button type="button" id="btnDelBook_${book.id}" class="btn btn-danger">Delete</button></td>
        </tr>
      `)
      $(`#btnDelBook_${book.id}`).click(deleteBook)
    })
  }).catch(e => console.log(`Failed to load book list ${e}`))
}

function getPublisher() {
  $('#publisher').empty()
  axios.get('/publisher.php')
  .then(res => {
    $('#publisher').append("<option selected></option>")
    res.data.forEach(pub => {
      $('.publisher-list').append(`
        <option>${pub.name}</option>
      `)
    })
  }).catch(e => console.log(e));
}

function searchBooks() {
  let title = $('#title').val()
  let author = $('#author').val()
  let publisher = $('#publisher').val()
  let year = $('#year').val()
  getBooks(`title=${title}&author=${author}&publisher=${publisher}&year=${year}`)
}

function deleteBook(e) {
  console.log(`/books.php?method=DELETE&id=${e.target.id.split('_')[1]}`)
  axios.get(`/books.php?method=DELETE&id=${e.target.id.split('_')[1]}`)
  .then(res => {
    console.log(res)
    getBooks()
  }).catch(e =>console.log(`Failed to delete book ${e}`))
}

function CreateBook() {
  let title = $('#insert-title').val()
  let author = $('#insert-author').val()
  let publisher = $('#insert-publisher').val()
  let year = $('#insert-year').val()
  axios.get(`/books.php?method=POST&title=${title}&author=${author}&publisher=${publisher}&year=${year}`)
  .then(res => {
    console.log(res)
    getBooks() 
  })
  
}

$(document).ready(() => {
  $('#btnSearch').click(searchBooks)
  $('#btnCreate').click(CreateBook)
  getPublisher()
  getBooks()
});