<?php
// Prevent direct access
if (explode('?', $_SERVER['REQUEST_URI'])[0] == '/db.php') header('Location: /');

include './config.php';

function InitDB()
{
  $dbi = NULL;
  try {
    $dbi = new PDO($GLOBALS['CON_STR'], DB_USR, DB_PASS);
  } catch (PDOException $e) {
    echo $e;
    die();
  }
  return $dbi;
}
