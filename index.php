<?php
session_start();

if (!isset($_SESSION['logged']) || !$_SESSION['logged']) header('Location: /login.php');

?>

<!doctype html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Library</title>
  <link rel="stylesheet" href="css/bootstrap.css">
</head>

<body>
  <div style="width: 300px; margin-left: 20px; margin-bottom: 50px; display: inline-block;">
    <label for="title">Titre</label>
    <input type="text" class="form-control" id="title">
    <label for="author">Auteur</label>
    <input type="text" class="form-control" id="author">
    <label for="publisher">Edition</label>
    <select id="publisher" class="form-control publisher-list">
      <option></option>
    </select>
    <label for="year">Année</label>
    <input type="text" class="form-control" id="year" name="year">
    <button id="btnSearch" class="btn btn-primary">Rechercher</button>
  </div>
  <?php if ($_SESSION['privilege'] == '1') { ?>
    <div style="width: 300px; margin-left: 20px; margin-bottom: 50px; display: inline-block;">
      <label for="insert-title">Titre</label>
      <input type="text" class="form-control" id="insert-title">
      <label for="insert-author">Auteur</label>
      <input type="text" class="form-control" id="insert-author">
      <label for="insert-publisher">Edition</label>
      <select id="insert-publisher" class="form-control publisher-list">
        <option></option>
      </select>
      <label for="insert-year">Année</label>
      <input type="text" class="form-control" id="insert-year" name="year">
      <button id="btnCreate" class="btn btn-primary">Créer</button>
    </div>
  <?php } ?>
  <a href="/login.php?logout=">Deconnexion</a>

  <table id="bookList" class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Titre</th>
        <th scope="col">Auteur</th>
        <th scope="col">Edition</th>
        <th scope="col">Année</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <script src="js/jquery.js"></script>
  <script src="js/axios.js"></script>
  <script src="js/index.js"></script>
</body>

</html>