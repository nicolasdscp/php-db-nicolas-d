<?php
// Prevent direct access
if (explode('?', $_SERVER['REQUEST_URI'])[0] == '/controllers/books.php') header('Location: /');

include 'models/books.php';

function GetBooks($reqParams)
{
  if (count($reqParams) == 0) {
    echo json_encode(DBGetAllBooks());
    return;
  }
  echo json_encode(DBSearchBooks($reqParams));
  return;
}

function DeleteBook($bookID)
{
  DBDeleteBook($bookID);
}

function CreateBook($reqParams)
{
  DBCreateBook($reqParams);
}
