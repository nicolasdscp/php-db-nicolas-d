<?php

// Prevent direct access
if (explode('?', $_SERVER['REQUEST_URI'])[0] == '/controllers/users.php') header('Location: /');

include 'models/users.php';

// Check user in DB
function CheckUser($login)
{
  $user = DBCheckUser($login);
  if (count($user) == 0) return false;
  // Session validation
  $_SESSION['logged'] = true;
  $_SESSION['privilege'] = intval($user[0]['privilege']);
  return true;
}
