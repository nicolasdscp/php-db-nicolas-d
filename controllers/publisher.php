<?php
// Prevent direct access
if (explode('?', $_SERVER['REQUEST_URI'])[0] == '/controllers/publisher.php') header('Location: /');

include 'models/publisher.php';

function GetPublisher() {
  echo json_encode(DBGetAllPublisher());
}