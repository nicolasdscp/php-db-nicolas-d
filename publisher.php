<?php
include 'controllers/publisher.php';

session_start();

// Unlogger user prevent
if (!isset($_SESSION['logged']) || !$_SESSION['logged']) header('Location: /login.php');

// Just return all publishers
GetPublisher();

